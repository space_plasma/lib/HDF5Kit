/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "HDF5KitTests.h"

#import <HDF5Kit/H5Type.h>
#import <HDF5Kit/TypeMap.h>

@interface HDF5KitTests (Type)

@end

@implementation HDF5KitTests (Type)
using hdf5::Type;
using hdf5::make_type;

- (void)testType_Native {
    try {
        Type t;
        XCTAssert(!t);

        t = make_type<char>();
        XCTAssert(H5T_NATIVE_CHAR == *t && t.size() == sizeof(char) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());
        t = make_type<unsigned char>();
        XCTAssert(H5T_NATIVE_UCHAR == *t && t.size() == sizeof(unsigned char) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());

        t = make_type<short>();
        XCTAssert(H5T_NATIVE_SHORT == *t && t.size() == sizeof(short) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());
        t = make_type<unsigned short>();
        XCTAssert(H5T_NATIVE_USHORT == *t && t.size() == sizeof(unsigned short) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());

        t = make_type<int>();
        XCTAssert(H5T_NATIVE_INT == *t && t.size() == sizeof(int) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());
        t = make_type<unsigned int>();
        XCTAssert(H5T_NATIVE_UINT == *t && t.size() == sizeof(unsigned int) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());

        t = make_type<long>();
        XCTAssert(H5T_NATIVE_LONG == *t && t.size() == sizeof(long) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());
        t = make_type<unsigned long>();
        XCTAssert(H5T_NATIVE_ULONG == *t && t.size() == sizeof(unsigned long) &&
                  H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());

        t = make_type<float>();
        XCTAssert(H5T_NATIVE_FLOAT == *t && t.size() == sizeof(float) &&
                  H5T_FLOAT == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());
        t = make_type<double>();
        XCTAssert(H5T_NATIVE_DOUBLE == *t && t.size() == sizeof(double) &&
                  H5T_FLOAT == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());

        Type const t2{t};
        t = t2;
        XCTAssert(H5Tequal(H5T_NATIVE_DOUBLE, *t) > 0 && t.size() == sizeof(double) &&
                  H5T_FLOAT == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed());
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testType_Derived {
    try {
        Type t1 = Type::compound(8);
        XCTAssert(H5T_COMPOUND == t1.class_() && t1.size() == 8);
        t1 = Type::opaque(8);
        XCTAssert(H5T_OPAQUE == t1.class_() && t1.size() == 8);

        t1 = make_type<int>().enumeration();
        XCTAssert(H5T_ENUM == t1.class_() && t1.size() == sizeof(int) &&
                  H5Tequal(*t1.super_(), H5T_NATIVE_INT) && t1.order() == H5T_ORDER_LE);

        t1 = make_type<double>().vlen();
        XCTAssert(H5T_VLEN == t1.class_() &&
                  H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE);

        using A = std::array<double, 3>;
        t1 = make_type<A>();
        XCTAssert(H5T_ARRAY == t1.class_() && t1.size() == sizeof(A) &&
                  H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE);

        using P = std::pair<double, double>;
        t1 = make_type<P>();
        XCTAssert(H5T_ARRAY == t1.class_() && t1.size() == sizeof(P) &&
                  H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE);

        using C = std::complex<double>;
        t1 = make_type<C>();
        XCTAssert(H5T_ARRAY == t1.class_() && t1.size() == sizeof(C) &&
                  H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE);
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end

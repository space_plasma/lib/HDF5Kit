/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "HDF5KitTests.h"
#import <HDF5Kit/H5Space.h>

#include <algorithm>

@interface HDF5KitTests (Space)

@end

@implementation HDF5KitTests (Space)
using hdf5::Space;
using hdf5::Extent;

- (void)testSpace {
    try {
        Space s, s2;
        XCTAssert(!s);

        Extent dims, max_dims;
        s = s2 = Space::scalar();
        XCTAssert(s.is_simple() && 0 == s.rank());
        std::tie(dims, max_dims) = s.simple_extent();
        XCTAssert(dims.empty() && max_dims.empty());
        s.select_all();
        s.select_none();

        dims = {1, 2, 3};
        max_dims = {3, 3, 3};
        s = s2 = Space::simple(dims, max_dims);
        XCTAssert(s.is_simple() && dims.rank() == s.rank());
        {
            auto const [e1, e2] = s.simple_extent();
            XCTAssert(e1.rank() == dims.rank() && std::equal(e1.begin(), e1.end(), dims.begin()));
            XCTAssert(e2.rank() == dims.rank() && std::equal(e2.begin(), e2.end(), max_dims.begin()));
        }
        s.select(H5S_SELECT_SET, {0, 0, 0}, dims);
        s.select(H5S_SELECT_SET, {0, 0, 0}, {1, 1, 1}, {1, 1, 1});
        s.select(H5S_SELECT_SET, {0, 0, 0}, {1, 1, 1}, {1, 1, 1}, dims);

        dims = {3, 3, 3};
        max_dims = {3, 3, 3};
        s = s2 = Space::simple(dims);
        XCTAssert(s.is_simple() && dims.rank() == s.rank());
        {
            auto const [e1, e2] = s.simple_extent();
            XCTAssert(e1.rank() == dims.rank() && std::equal(e1.begin(), e1.end(), dims.begin()));
            XCTAssert(e2.rank() == dims.rank() && std::equal(e2.begin(), e2.end(), max_dims.begin()));
        }
        s.select(H5S_SELECT_SET, {0, 0, 0}, dims);
        s.select(H5S_SELECT_SET, {0, 0, 0}, {1, 1, 1}, {1, 1, 1});
        s.select(H5S_SELECT_SET, {0, 0, 0}, {1, 1, 1}, {1, 1, 1}, dims);

        dims = {33, 32, 13};
        max_dims = {H5S_UNLIMITED, H5S_UNLIMITED, H5S_UNLIMITED};
        s = s2 = Space::simple(dims, nullptr);
        XCTAssert(s.is_simple() && dims.rank() == s.rank());
        {
            auto const [e1, e2] = s.simple_extent();
            XCTAssert(e1.rank() == dims.rank() && std::equal(e1.begin(), e1.end(), dims.begin()));
            XCTAssert(e2.rank() == dims.rank() && std::equal(e2.begin(), e2.end(), max_dims.begin()));
        }
        s.select(H5S_SELECT_SET, {0, 0, 0}, dims);
        s.select(H5S_SELECT_SET, {0, 0, 0}, {1, 1, 1}, {1, 1, 1});
        s.select(H5S_SELECT_SET, {0, 0, 0}, {1, 1, 1}, {1, 1, 1}, dims);
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end

/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "HDF5KitTests.h"

#import <HDF5Kit/Extent.h>
#include "println.h"

#include <array>
#include <algorithm>
#include <iostream>

@interface HDF5KitTests (Extent)

@end

@implementation HDF5KitTests (Extent)

- (void)testExtent {
    using hdf5::Extent;

    constexpr std::array<unsigned, 6> N{3, 4, 5, 6, 7, 8};

    try {
        {
            constexpr Extent ex;
            static_assert(ex.empty() && ex.rank() == 0 && ex.size() == 0 && ex.max_size() == H5S_MAX_RANK);
            static_assert(ex.data() == ex.cbegin() && ex.data() == ex.cend());
            static_assert(ex.data() == ex.crbegin().base() && ex.data() == ex.crend().base());

            println(std::cout, ex);
        }
        {

            constexpr Extent ex{N[0]};
            static_assert(!ex.empty() && ex.rank() == 1 && ex.size() == ex.rank() && ex.max_size() == H5S_MAX_RANK);
            static_assert(*ex.data() == *ex.cbegin() && std::distance(ex.cbegin(), ex.cend()) == ex.size());
            static_assert(*(ex.crend() - 1) == *ex.data() && std::distance(ex.crbegin(), ex.crend()) == ex.size());
            static_assert(N[0] == *ex.data() && N[0] == ex.front() && N[ex.rank() - 1] == ex.back());
#if !defined(DEBUG)
            static_assert(N[0] == ex[0] && ex.data() == &ex[0]);
#else
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex[i] && ex.data() + i == &ex[i], @"ex[%d] = %lld", i, ex[i]);
            }
#endif
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex.at(i) && ex.data() + i == &ex.at(i), @"ex.at(%d) = %lld", i, ex.at(i));
            }

            println(std::cout, ex);
        }
        {

            constexpr Extent ex{N[0], N[1]};
            static_assert(!ex.empty() && ex.rank() == 2 && ex.size() == ex.rank() && ex.max_size() == H5S_MAX_RANK);
            static_assert(*ex.data() == *ex.cbegin() && std::distance(ex.cbegin(), ex.cend()) == ex.size());
            static_assert(*(ex.crend() - 1) == *ex.data() && std::distance(ex.crbegin(), ex.crend()) == ex.size());
            static_assert(N[0] == *ex.data() && N[0] == ex.front() && N[ex.rank() - 1] == ex.back());
#if !defined(DEBUG)
            static_assert(N[0] == ex[0] && ex.data() == &ex[0]);
#else
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex[i] && ex.data() + i == &ex[i], @"ex[%d] = %lld", i, ex[i]);
            }
#endif
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex.at(i) && ex.data() + i == &ex.at(i), @"ex.at(%d) = %lld", i, ex.at(i));
            }

            println(std::cout, ex);
        }
        {

            constexpr Extent ex{N[0], N[1], N[2]};
            static_assert(!ex.empty() && ex.rank() == 3 && ex.size() == ex.rank() && ex.max_size() == H5S_MAX_RANK);
            static_assert(*ex.data() == *ex.cbegin() && std::distance(ex.cbegin(), ex.cend()) == ex.size());
            static_assert(*(ex.crend() - 1) == *ex.data() && std::distance(ex.crbegin(), ex.crend()) == ex.size());
            static_assert(N[0] == *ex.data() && N[0] == ex.front() && N[ex.rank() - 1] == ex.back());
#if !defined(DEBUG)
            static_assert(N[0] == ex[0] && ex.data() == &ex[0]);
#else
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex[i] && ex.data() + i == &ex[i], @"ex[%d] = %lld", i, ex[i]);
            }
#endif
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex.at(i) && ex.data() + i == &ex.at(i), @"ex.at(%d) = %lld", i, ex.at(i));
            }

            println(std::cout, ex);
        }
        {

            constexpr Extent ex{N[0], N[1], N[2], N[3]};
            static_assert(!ex.empty() && ex.rank() == 4 && ex.size() == ex.rank() && ex.max_size() == H5S_MAX_RANK);
            static_assert(*ex.data() == *ex.cbegin() && std::distance(ex.cbegin(), ex.cend()) == ex.size());
            static_assert(*(ex.crend() - 1) == *ex.data() && std::distance(ex.crbegin(), ex.crend()) == ex.size());
            static_assert(N[0] == *ex.data() && N[0] == ex.front() && N[ex.rank() - 1] == ex.back());
#if !defined(DEBUG)
            static_assert(N[0] == ex[0] && ex.data() == &ex[0]);
#else
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex[i] && ex.data() + i == &ex[i], @"ex[%d] = %lld", i, ex[i]);
            }
#endif
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex.at(i) && ex.data() + i == &ex.at(i), @"ex.at(%d) = %lld", i, ex.at(i));
            }

            println(std::cout, ex);
        }
        {

            constexpr Extent ex{N[0], N[1], N[2], N[3], N[4]};
            static_assert(!ex.empty() && ex.rank() == 5 && ex.size() == ex.rank() && ex.max_size() == H5S_MAX_RANK);
            static_assert(*ex.data() == *ex.cbegin() && std::distance(ex.cbegin(), ex.cend()) == ex.size());
            static_assert(*(ex.crend() - 1) == *ex.data() && std::distance(ex.crbegin(), ex.crend()) == ex.size());
            static_assert(N[0] == *ex.data() && N[0] == ex.front() && N[ex.rank() - 1] == ex.back());
#if !defined(DEBUG)
            static_assert(N[0] == ex[0] && ex.data() == &ex[0]);
#else
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex[i] && ex.data() + i == &ex[i], @"ex[%d] = %lld", i, ex[i]);
            }
#endif
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex.at(i) && ex.data() + i == &ex.at(i), @"ex.at(%d) = %lld", i, ex.at(i));
            }

            println(std::cout, ex);
        }
        {

            constexpr Extent ex{N[0], N[1], N[2], N[3], N[4], N[5]};
            static_assert(!ex.empty() && ex.rank() == 6 && ex.size() == ex.rank() && ex.max_size() == H5S_MAX_RANK);
            static_assert(*ex.data() == *ex.cbegin() && std::distance(ex.cbegin(), ex.cend()) == ex.size());
            static_assert(*(ex.crend() - 1) == *ex.data() && std::distance(ex.crbegin(), ex.crend()) == ex.size());
            static_assert(N[0] == *ex.data() && N[0] == ex.front() && N[ex.rank() - 1] == ex.back());
#if !defined(DEBUG)
            static_assert(N[0] == ex[0] && ex.data() == &ex[0]);
#else
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex[i] && ex.data() + i == &ex[i], @"ex[%d] = %lld", i, ex[i]);
            }
#endif
            for (unsigned i = 0; i < ex.size(); ++i) {
                XCTAssert(N.at(i) == ex.at(i) && ex.data() + i == &ex.at(i), @"ex.at(%d) = %lld", i, ex.at(i));
            }

            println(std::cout, ex);
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        Extent e1{begin(N), end(N)};
        XCTAssert(N.size() == e1.rank());
        XCTAssert(std::equal(begin(N), end(N), e1.begin()));
        XCTAssert(std::equal(rbegin(N), rend(N), e1.rbegin()));
        XCTAssert(N.front() == e1.front() && N.back() == e1.back());
        for (unsigned i = 0; i < e1.size(); ++i) {
            XCTAssert(N.at(i) == e1[i] && e1.data() + i == &e1[i], @"e1[%d] = %lld", i, e1[i]);
            XCTAssert(N.at(i) == e1.at(i) && e1.data() + i == &e1.at(i), @"e1.at(%d) = %lld", i, e1.at(i));
        }

        auto e2 = Extent::unlimited(10);
        XCTAssert(10 == e2.rank());
        for (unsigned i = 0; i < e2.size(); ++i) {
            XCTAssert(H5S_UNLIMITED == e2[i]);
            XCTAssert(H5S_UNLIMITED == e2.at(i));
        }

        e2.fill(1);
        std::swap(e1, e2);
        XCTAssert(10 == e1.rank());
        for (unsigned i = 0; i < e1.size(); ++i) {
            XCTAssert(1 == e1[i]);
            XCTAssert(1 == e1.at(i));
        }
        for (unsigned i = 0; i < e2.size(); ++i) {
            XCTAssert(N.at(i) == e2[i] && e2.data() + i == &e2[i], @"e2[%d] = %lld", i, e2[i]);
            XCTAssert(N.at(i) == e2.at(i) && e2.data() + i == &e2.at(i), @"e2.at(%d) = %lld", i, e2.at(i));
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end

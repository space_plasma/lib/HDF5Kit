/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "HDF5KitTests.h"

#import <HDF5Kit/H5PList.h>

#include <algorithm>

@interface HDF5KitTests (PList)

@end

@implementation HDF5KitTests (PList)

- (void)testPList {
    using hdf5::PList;
    using hdf5::Extent;

    try {
        PList const p1;
        XCTAssert(!!p1 && H5P_DEFAULT == *p1);

        PList p2;
        p2 = PList::tcpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_DATATYPE_CREATE) > 0);
        p2 = PList::tapl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_DATATYPE_ACCESS) > 0);
        p2 = PList::acpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_ATTRIBUTE_CREATE) > 0);
        p2 = PList::gcpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_GROUP_CREATE) > 0);
        p2 = PList::gapl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_GROUP_ACCESS) > 0);
        p2 = PList::fcpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_FILE_CREATE) > 0);
        p2 = PList::fapl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_FILE_ACCESS) > 0);
        p2 = PList::lcpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_LINK_CREATE) > 0);
        p2 = PList::lapl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_LINK_ACCESS) > 0);
        p2 = PList::dcpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_DATASET_CREATE) > 0);
        p2 = PList::dapl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_DATASET_ACCESS) > 0);
        p2 = PList::dxpl();
        XCTAssert(H5Pequal(H5Pget_class(*p2), H5P_DATASET_XFER) > 0);

        constexpr Extent dims{4, 5, 6};
        p2 = PList::dcpl();
        p2.set_chunk(dims);
        auto const chunk = p2.chunk();
        XCTAssert(dims.rank() == chunk.rank() && std::equal(dims.begin(), dims.end(), chunk.begin()));
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end

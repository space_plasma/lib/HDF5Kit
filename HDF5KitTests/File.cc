/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "HDF5KitTests.h"
#include "catch2/catch.hpp"

#include <HDF5Kit/HDF5Kit.h>

#include <array>
#include <string>
#include <vector>

using namespace hdf5;

namespace {
constexpr char filepath[] = "./test.h5";
}

TEST_CASE("Test hdf5::File operations", "[hdf5::File]")
{
    std::vector<double> const    d{ 1, 2, 3, 4, 5 };
    std::array<double, 3> const  v{ 1, 2, 3 };
    std::pair<int, double> const p{ 1, 3 };

    // hdf5 create
    try {
        std::string s;

        // file
        //
        File file;
        REQUIRE_FALSE(file);

        File f1{ File::trunc_tag{}, filepath, PList::fapl(), PList::fcpl() };
        file = std::move(f1);

        s = file.file_path();
        INFO("File::file_path is " << s);
        REQUIRE(s == filepath);

        s = file.path();
        INFO("File::path is " << s);
        REQUIRE(s == "/");

        CHECK_FALSE(!file.cpl());
        CHECK_FALSE(!file.apl());
        file.flush();

        Group root{ std::move(file) };
        root.flush();
        CHECK_FALSE(!root.cpl());

        // group attribute
        //
        Attribute gattr
            = root.attribute("attr", make_type<double>(), Space::scalar(), PList{}, PList::acpl());
        gattr.write(*d.data());
        gattr.flush();
        REQUIRE((gattr.name() == "attr" && 0 == gattr.space().rank()));
        REQUIRE(H5Tequal(H5T_NATIVE_DOUBLE, *gattr.type()) > 0);

        gattr = root.attribute("attr_v", make_type<decltype(v)>(), Space::scalar(), PList{},
                               PList::acpl());
        gattr.write(v);
        gattr.flush();
        REQUIRE(0 == gattr.space().rank());
        REQUIRE(H5Tequal(*make_type<decltype(v)>(), *gattr.type()) > 0);

        gattr = root.attribute("attr_a", make_type<double>(), Space::simple(v.size()), PList{},
                               PList::acpl());
        gattr.write(v.data(), make_type<double>());
        gattr.flush();
        {
            auto const [dims, _] = gattr.space().simple_extent();
            REQUIRE((dims.rank() == 1 && dims[0] == v.size()));
        }

        // subgroup
        //
        Group group = root.group("group", PList::gapl(), PList::gcpl(), PList::lcpl());
        REQUIRE(group.path() == "/group");

        // dataset
        //
        Dataset dset  = group.dataset("dset", make_type<double>(), Space::simple(d.size()),
                                      PList::dapl(), PList::dcpl(), PList::lcpl());
        Space   space = dset.space();
        space.select_all();
        dset.write(space, d.data(), space, PList::dxpl());
        dset.flush();
        REQUIRE(dset.path() == "/group/dset");
        REQUIRE(H5Tequal(H5T_NATIVE_DOUBLE, *dset.type()) > 0);

        // chunked dataset
        //
        PList dcpl = PList::dcpl();
        dcpl.set_chunk(64);
        Dataset chunk = group.dataset("chunk", make_type<double>(), Space::simple(0, nullptr),
                                      PList::dapl(), dcpl);
        chunk.set_extent(d.size());
        space = chunk.space();
        space.select_all();
        chunk.write(space, d.data(), space);

        // scalar vector dataset
        //
        dset  = group.dataset("vector", make_type<decltype(v)>(), Space::scalar());
        space = dset.space();
        space.select_all();
        dset.write(space, &v, space);
        REQUIRE(H5Tequal(*make_type<decltype(v)>(), *dset.type()) > 0);

        // scalar compound dataset
        //
        dset  = group.dataset("compound", make_type<decltype(p)>(), Space::scalar());
        space = dset.space();
        space.select_all();
        dset.write(space, &p, space);
        REQUIRE(H5Tequal(*make_type<decltype(p)>(), *dset.type()) > 0);
    } catch (std::exception &e) {
        INFO("Exception thrown: " << e.what());
        REQUIRE(false);
    }

    // hdf5 open
    try {
        std::string s;

        // file
        //
        File f1{ File::rdonly_tag{}, filepath, PList::fapl() };
        File file{ std::move(f1) };

        s = file.file_path();
        INFO("File::file_path is " << s);
        REQUIRE(s == filepath);

        s = file.path();
        INFO("File::path is " << s);
        REQUIRE(s == "/");

        Group &root = file;

        // group attribute
        //
        Attribute gattr = root.attribute("attr", PList{});
        s               = gattr.name();
        REQUIRE((gattr.name() == "attr" && 0 == gattr.space().rank()));
        REQUIRE(H5Tequal(H5T_NATIVE_DOUBLE, *gattr.type()) > 0);
        if (gattr.space().rank() == 0) {
            double const x = gattr.read<double>();
            INFO("gattr.read<double>() = " << x);
            CHECK(x == d[0]);
        }

        gattr = root.attribute("attr_v");
        REQUIRE(0 == gattr.space().rank());
        REQUIRE(H5Tequal(*make_type<decltype(v)>(), *gattr.type()) > 0);
        if (gattr.space().rank() == 0) {
            auto v2 = v;
            gattr.read(v2);
            for (unsigned i = 0; i < v.size(); ++i) {
                INFO("i = " << i << "; gattr.read(v2) = " << v2[i]);
                CHECK(v[i] == v2[i]);
            }
        }

        // subgroup
        //
        Group group = root.group("group", PList::gapl());
        s           = group.path();
        REQUIRE(s == "/group");

        // dataset
        //
        Dataset dset = group.dataset("dset", PList::dapl());
        s            = dset.path();
        REQUIRE(s == "/group/dset");
        REQUIRE(H5Tequal(H5T_NATIVE_DOUBLE, *dset.type()) > 0);

        Space space = dset.space();
        auto  dims  = space.simple_extent().first;

        REQUIRE((dims.size() == 1 && dims[0] == d.size()));
        std::vector<double> buf(d.size());
        space.select_all();
        dset.read(space, buf.data(), space, PList::dxpl());
        for (unsigned i = 0; i < buf.size(); ++i) {
            CHECK(d[i] == buf[i]);
        }

        // chunked dataset
        //
        Dataset chunk = group.dataset("chunk");
        space         = chunk.space();
        dims          = space.simple_extent().first;
        REQUIRE((dims.size() == 1 && dims[0] == d.size()));
        buf.resize(d.size());
        space.select_all();
        chunk.read(space, buf.data(), space, PList::dxpl());
        for (unsigned i = 0; i < buf.size(); ++i) {
            CHECK(d[i] == buf[i]);
        }

        // scalar vector dataset
        //
        dset = group.dataset("vector");
        REQUIRE(H5Tequal(*make_type<decltype(v)>(), *dset.type()) > 0);
        (space = dset.space()).select_all();
        auto v2 = v;
        dset.read(space, &v2, space);
        for (unsigned i = 0; i < v.size(); ++i) {
            INFO("i = " << i << "; dset.read(space, &v2, space) = " << v2[i]);
            CHECK(v[i] == v2[i]);
        }

        // scalar compound dataset
        //
        dset = group.dataset("compound");
        REQUIRE(H5Tequal(*make_type<decltype(p)>(), *dset.type()) > 0);
        (space = dset.space()).select_all();
        auto p2 = p;
        dset.read(space, &p2, space);
        INFO("dset.read(space, &p2, space) = {.first = " << p2.first
                                                         << ", .second = " << p2.second);
        CHECK(p == p2);
    } catch (std::exception &e) {
        INFO("Exception thrown: " << e.what());
        REQUIRE(false);
    }
}

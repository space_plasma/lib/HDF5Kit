/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "HDF5KitTests.h"
#include "catch2/catch.hpp"

#include <HDF5Kit/H5PList.h>

#include <algorithm>

using hdf5::Extent;
using hdf5::PList;

TEST_CASE("Test hdf5::PList", "[hdf5::PList]")
{
    try {
        PList const p1;
        REQUIRE((static_cast<bool>(p1) && H5P_DEFAULT == *p1));

        PList p2;
        REQUIRE_NOTHROW(p2 = PList::tcpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_DATATYPE_CREATE) > 0);
        REQUIRE_NOTHROW(p2 = PList::tapl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_DATATYPE_ACCESS) > 0);
        REQUIRE_NOTHROW(p2 = PList::acpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_ATTRIBUTE_CREATE) > 0);
        REQUIRE_NOTHROW(p2 = PList::gcpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_GROUP_CREATE) > 0);
        REQUIRE_NOTHROW(p2 = PList::gapl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_GROUP_ACCESS) > 0);
        REQUIRE_NOTHROW(p2 = PList::fcpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_FILE_CREATE) > 0);
        REQUIRE_NOTHROW(p2 = PList::fapl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_FILE_ACCESS) > 0);
        REQUIRE_NOTHROW(p2 = PList::lcpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_LINK_CREATE) > 0);
        REQUIRE_NOTHROW(p2 = PList::lapl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_LINK_ACCESS) > 0);
        REQUIRE_NOTHROW(p2 = PList::dcpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_DATASET_CREATE) > 0);
        REQUIRE_NOTHROW(p2 = PList::dapl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_DATASET_ACCESS) > 0);
        REQUIRE_NOTHROW(p2 = PList::dxpl());
        CHECK(H5Pequal(H5Pget_class(*p2), H5P_DATASET_XFER) > 0);

        constexpr Extent dims{ 4, 5, 6 };
        REQUIRE_NOTHROW(p2 = PList::dcpl());
        REQUIRE_NOTHROW(p2.set_chunk(dims));
        decltype(p2.chunk()) chunk;
        REQUIRE_NOTHROW(chunk = p2.chunk());
        CHECK((dims.rank() == chunk.rank() && std::equal(dims.begin(), dims.end(), chunk.begin())));
    } catch (std::exception const &e) {
        INFO("Exception thrown: " << e.what());
        REQUIRE(false);
    }
}

/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "HDF5KitTests.h"
#import <HDF5Kit/HDF5Kit.h>

#include <string>
#include <vector>
#include <array>

@interface HDF5KitTests (File)

@end

@implementation HDF5KitTests (File)
using namespace hdf5;

- (void)testFile {
    NSString *filepath = [NSHomeDirectory() stringByAppendingPathComponent:@"Downloads/test.h5"];
    std::vector<double> const d{1, 2, 3, 4, 5};
    std::array<double, 3> const v{1, 2, 3};

    // hdf5 create
    try {
        std::string s;

        // file
        //
        File f1{File::trunc_tag{}, filepath.UTF8String, PList::fapl(), PList::fcpl()};
        File file;
        XCTAssert(!file);

        file = std::move(f1);
        s = file.file_path();
        XCTAssert(s == filepath.UTF8String, @"%s", s.c_str());
        s = file.path();
        XCTAssert(s == "/", @"%s", s.c_str());
        XCTAssert(!!file.cpl() && !!file.apl());
        file.flush();

        Group root{std::move(file)};
        root.flush();
        XCTAssert(!!root.cpl());

        // group attribute
        //
        Attribute gattr = root.attribute("attr", make_type<double>(), Space::scalar(), PList{}, PList::acpl());
        gattr.write(*d.data());
        gattr.flush();
        XCTAssert(gattr.name() == "attr" && 0 == gattr.space().rank());
        XCTAssert(H5Tequal(H5T_NATIVE_DOUBLE, *gattr.type()) > 0);

        gattr = root.attribute("attr_v", make_type<decltype(v)>(), Space::scalar(), PList{}, PList::acpl());
        gattr.write(v);
        gattr.flush();
        XCTAssert(0 == gattr.space().rank());
        XCTAssert(H5Tequal(*make_type<decltype(v)>(), *gattr.type()) > 0);

        gattr = root.attribute("attr_a", make_type<double>(), Space::simple(v.size()), PList{}, PList::acpl());
        gattr.write(v.data(), make_type<double>());
        gattr.flush();
        {
            auto const [dims, _] = gattr.space().simple_extent();
            XCTAssert(dims.rank() == 1 && dims[0] == v.size());
        }

        // subgroup
        //
        Group group = root.group("group", PList::gapl(), PList::gcpl(), PList::lcpl());
        XCTAssert(group.path() == "/group");

        // dataset
        //
        Dataset dset = group.dataset("dset", make_type<double>(), Space::simple(d.size()), PList::dapl(), PList::dcpl(), PList::lcpl());
        Space space = dset.space();
        space.select_all();
        dset.write(space, d.data(), space, PList::dxpl());
        dset.flush();
        XCTAssert(dset.path() == "/group/dset");
        XCTAssert(H5Tequal(H5T_NATIVE_DOUBLE, *dset.type()) > 0);

        // chunked dataset
        //
        PList dcpl = PList::dcpl();
        dcpl.set_chunk(64);
        Dataset chunk = group.dataset("chunk", make_type<double>(), Space::simple(0, nullptr), PList::dapl(), dcpl);
        chunk.set_extent(d.size());
        space = chunk.space();
        space.select_all();
        chunk.write(space, d.data(), space);

        // scalar vector dataset
        //
        dset = group.dataset("vector", make_type<decltype(v)>(), Space::scalar());
        space = dset.space();
        space.select_all();
        dset.write(space, &v, space);
        XCTAssert(H5Tequal(*make_type<decltype(v)>(), *dset.type()) > 0);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // hdf5 open
    try {
        std::string s;

        // file
        //
        File f1{File::rdonly_tag{}, filepath.UTF8String, PList::fapl()},
        file{std::move(f1)};
        s = file.file_path();
        XCTAssert(s == filepath.UTF8String, @"%s", s.c_str());
        s = file.path();
        XCTAssert(s == "/", @"%s", s.c_str());
        Group &root = file;

        // group attribute
        //
        Attribute gattr = root.attribute("attr", PList{});
        s = gattr.name();
        XCTAssert(gattr.name() == "attr" && 0 == gattr.space().rank());
        XCTAssert(H5Tequal(H5T_NATIVE_DOUBLE, *gattr.type()) > 0);
        if (gattr.space().rank() == 0) {
            double const x = gattr.read<double>();
            XCTAssert(x == d[0], @"x = %f", x);
        }
        gattr = root.attribute("attr_v");
        XCTAssert(0 == gattr.space().rank());
        XCTAssert(H5Tequal(*make_type<decltype(v)>(), *gattr.type()) > 0);
        if (gattr.space().rank() == 0) {
            auto v2 = v;
            gattr.read(v2);
            for (unsigned i = 0; i < v.size(); ++i) {
                XCTAssert(v[i] == v2[i], @"v2[%d] = %f", i, v2[i]);
            }
        }

        // subgroup
        //
        Group group = root.group("group", PList::gapl());
        s = group.path();
        XCTAssert(s == "/group");

        // dataset
        //
        Dataset dset = group.dataset("dset", PList::dapl());
        s = dset.path();
        XCTAssert(s == "/group/dset");
        XCTAssert(H5Tequal(H5T_NATIVE_DOUBLE, *dset.type()) > 0);

        Space space = dset.space();
        auto dims = space.simple_extent().first;

        XCTAssert(dims.size() == 1 && dims[0] == d.size());
        std::vector<double> buf(d.size());
        space.select_all();
        dset.read(space, buf.data(), space, PList::dxpl());
        for (unsigned i = 0; i < buf.size(); ++i) {
            XCTAssert(d[i] == buf[i]);
        }

        // chunked dataset
        //
        Dataset chunk = group.dataset("chunk");
        space = chunk.space();
        dims = space.simple_extent().first;
        XCTAssert(dims.size() == 1 && dims[0] == d.size());
        buf.resize(d.size());
        space.select_all();
        chunk.read(space, buf.data(), space, PList::dxpl());
        for (unsigned i = 0; i < buf.size(); ++i) {
            XCTAssert(d[i] == buf[i]);
        }

        // scalar vector dataset
        //
        dset = group.dataset("vector");
        XCTAssert(H5Tequal(*make_type<decltype(v)>(), *dset.type()) > 0);
        (space = dset.space()).select_all();
        auto v2 = v;
        dset.read(space, &v2, space);
        for (unsigned i = 0; i < v.size(); ++i) {
            XCTAssert(v[i] == v2[i], @"v2[%d] = %f", i, v2[i]);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end

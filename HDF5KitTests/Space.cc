/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "HDF5KitTests.h"
#include "catch2/catch.hpp"

#include <HDF5Kit/H5Space.h>

#include <algorithm>
#include <tuple>
#include <utility>

using hdf5::Extent;
using hdf5::Space;

TEST_CASE("Test hdf5::Space", "[hdf5::Space]")
{
    try {
        Space s, s2;
        REQUIRE_FALSE(s);

        Extent dims, max_dims;
        s = s2 = Space::scalar();
        REQUIRE((s.is_simple() && 0 == s.rank()));
        std::tie(dims, max_dims) = s.simple_extent();
        REQUIRE((dims.empty() && max_dims.empty()));

        REQUIRE_NOTHROW(s.select_all());
        REQUIRE_NOTHROW(s.select_none());

        dims     = { 1, 2, 3 };
        max_dims = { 3, 3, 3 };
        REQUIRE_NOTHROW(s = s2 = Space::simple(dims, max_dims));
        REQUIRE((s.is_simple() && dims.rank() == s.rank()));
        {
            auto const [e1, e2] = s.simple_extent();
            REQUIRE((e1.rank() == dims.rank() && std::equal(e1.begin(), e1.end(), dims.begin())));
            REQUIRE(
                ((e2.rank() == dims.rank() && std::equal(e2.begin(), e2.end(), max_dims.begin()))));
        }
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, dims));
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }));
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }, dims));

        dims     = { 3, 3, 3 };
        max_dims = { 3, 3, 3 };
        REQUIRE_NOTHROW(s = s2 = Space::simple(dims));
        REQUIRE((s.is_simple() && dims.rank() == s.rank()));
        {
            auto const [e1, e2] = s.simple_extent();
            REQUIRE((e1.rank() == dims.rank() && std::equal(e1.begin(), e1.end(), dims.begin())));
            REQUIRE(
                ((e2.rank() == dims.rank() && std::equal(e2.begin(), e2.end(), max_dims.begin()))));
        }
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, dims));
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }));
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }, dims));

        dims     = { 33, 32, 13 };
        max_dims = { H5S_UNLIMITED, H5S_UNLIMITED, H5S_UNLIMITED };
        REQUIRE_NOTHROW(s = s2 = Space::simple(dims, nullptr));
        REQUIRE((s.is_simple() && dims.rank() == s.rank()));
        {
            auto const [e1, e2] = s.simple_extent();
            REQUIRE((e1.rank() == dims.rank() && std::equal(e1.begin(), e1.end(), dims.begin())));
            REQUIRE(
                ((e2.rank() == dims.rank() && std::equal(e2.begin(), e2.end(), max_dims.begin()))));
        }
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, dims));
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }));
        REQUIRE_NOTHROW(s.select(H5S_SELECT_SET, { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }, dims));
    } catch (std::exception const &e) {
        INFO("Exception thrown: " << e.what());
        REQUIRE(false);
    }
}

/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "HDF5KitTests.h"
#include "catch2/catch.hpp"

#include <HDF5Kit/H5Type.h>
#include <HDF5Kit/TypeMap.h>

using hdf5::make_type;
using hdf5::Type;

TEST_CASE("Test hdf5::Type::Native", "[hdf5::Type::Native]")
{
    try {
        Type t;
        REQUIRE_FALSE(t);

        REQUIRE_NOTHROW(t = make_type<char>());
        REQUIRE((H5T_NATIVE_CHAR == *t && t.size() == sizeof(char) && H5T_INTEGER == t.class_()
                 && t.order() == H5T_ORDER_LE && !t.is_committed()));
        REQUIRE_NOTHROW(t = make_type<unsigned char>());
        REQUIRE((H5T_NATIVE_UCHAR == *t && t.size() == sizeof(unsigned char)
                 && H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed()));

        REQUIRE_NOTHROW(t = make_type<short>());
        REQUIRE((H5T_NATIVE_SHORT == *t && t.size() == sizeof(short) && H5T_INTEGER == t.class_()
                 && t.order() == H5T_ORDER_LE && !t.is_committed()));
        REQUIRE_NOTHROW(t = make_type<unsigned short>());
        REQUIRE((H5T_NATIVE_USHORT == *t && t.size() == sizeof(unsigned short)
                 && H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed()));

        REQUIRE_NOTHROW(t = make_type<int>());
        REQUIRE((H5T_NATIVE_INT == *t && t.size() == sizeof(int) && H5T_INTEGER == t.class_()
                 && t.order() == H5T_ORDER_LE && !t.is_committed()));
        REQUIRE_NOTHROW(t = make_type<unsigned int>());
        REQUIRE((H5T_NATIVE_UINT == *t && t.size() == sizeof(unsigned int)
                 && H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed()));

        REQUIRE_NOTHROW(t = make_type<long>());
        REQUIRE((H5T_NATIVE_LONG == *t && t.size() == sizeof(long) && H5T_INTEGER == t.class_()
                 && t.order() == H5T_ORDER_LE && !t.is_committed()));
        REQUIRE_NOTHROW(t = make_type<unsigned long>());
        REQUIRE((H5T_NATIVE_ULONG == *t && t.size() == sizeof(unsigned long)
                 && H5T_INTEGER == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed()));

        REQUIRE_NOTHROW(t = make_type<float>());
        REQUIRE((H5T_NATIVE_FLOAT == *t && t.size() == sizeof(float) && H5T_FLOAT == t.class_()
                 && t.order() == H5T_ORDER_LE && !t.is_committed()));
        REQUIRE_NOTHROW(t = make_type<double>());
        REQUIRE((H5T_NATIVE_DOUBLE == *t && t.size() == sizeof(double) && H5T_FLOAT == t.class_()
                 && t.order() == H5T_ORDER_LE && !t.is_committed()));

        Type const t2{ t };
        t = t2;
        REQUIRE((H5Tequal(H5T_NATIVE_DOUBLE, *t) > 0 && t.size() == sizeof(double)
                 && H5T_FLOAT == t.class_() && t.order() == H5T_ORDER_LE && !t.is_committed()));
    } catch (std::exception const &e) {
        INFO("Exception thrown: " << e.what());
        REQUIRE(false);
    }
}

struct S {
    int                    a;
    std::pair<double, int> b;
};

TEST_CASE("Test hdf5::Type::Drived", "[hdf5::Type::Drived]")
{
    try {
        Type t1;

        REQUIRE_NOTHROW(t1 = Type::compound(sizeof(S)));
        REQUIRE((H5T_COMPOUND == t1.class_() && t1.size() == sizeof(S)));
        REQUIRE_NOTHROW(t1.insert("a", HOFFSET(S, a), make_type(S{}.a)));
        REQUIRE_NOTHROW(t1.insert("b", HOFFSET(S, b), make_type(S{}.b)));

        REQUIRE_NOTHROW(t1 = Type::opaque(8));
        REQUIRE((H5T_OPAQUE == t1.class_() && t1.size() == 8));

        REQUIRE_NOTHROW(t1 = make_type<int>().enumeration());
        REQUIRE((H5T_ENUM == t1.class_() && t1.size() == sizeof(int)
                 && H5Tequal(*t1.super_(), H5T_NATIVE_INT) && t1.order() == H5T_ORDER_LE));

        REQUIRE_NOTHROW(t1 = make_type<double>().vlen());
        REQUIRE((H5T_VLEN == t1.class_() && H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE)
                 && t1.order() == H5T_ORDER_LE));

        using A = std::array<double, 3>;
        REQUIRE_NOTHROW(t1 = make_type<A>());
        REQUIRE((H5T_ARRAY == t1.class_() && t1.size() == sizeof(A)
                 && H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE));

        using P = std::pair<double, double>;
        REQUIRE_NOTHROW(t1 = make_type<P>());
        REQUIRE((H5T_ARRAY == t1.class_() && t1.size() == sizeof(P)
                 && H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE));

        using C = std::complex<double>;
        REQUIRE_NOTHROW(t1 = make_type<C>());
        REQUIRE((H5T_ARRAY == t1.class_() && t1.size() == sizeof(C)
                 && H5Tequal(*t1.super_(), H5T_NATIVE_DOUBLE) && t1.order() == H5T_ORDER_LE));

        using U = std::pair<char, double>;
        REQUIRE_NOTHROW(t1 = make_type<U>());
        REQUIRE((H5T_COMPOUND == t1.class_() && t1.size() == sizeof(U)));
    } catch (std::exception const &e) {
        INFO("Exception thrown: " << e.what());
        REQUIRE(false);
    }
}

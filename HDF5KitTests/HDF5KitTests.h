/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5KitTests_h
#define HDF5KitTests_h

#if defined(__APPLE__) && defined(__OBJC__)

#import <XCTest/XCTest.h>

@interface HDF5KitTests : XCTestCase

@end

#endif

#define HDF5KIT_INLINE_VERSION 1

#if defined(__APPLE__)
#include <hdf5/hdf5.h>
#else
#include <hdf5.h>
#endif

#endif /* HDF5KitTests_h */

/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5Group_h
#define HDF5Kit_H5Group_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/H5Attribute.h>
#include <HDF5Kit/H5Dataset.h>
#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5PList.h>

#include <string>
#include <utility>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class Space;
class Type;

/**
 @brief Wrapper for HDF5 attribute.
 */
class Group : public Object {
    std::string m_path{};
    bool        is_file{};

    Group(decltype(nullptr), hid_t handle);

public:
    /// @{
    Group() noexcept         = default;
    Group(Group &&) noexcept = default;
    Group &operator=(Group &&) noexcept = default;
    /// @}
private: // to be used by Group
    Group(Group const &parent, char const *name, PList const &gapl, PList const &gcpl,
          PList const &lcpl);                                        // creation
    Group(Group const &parent, char const *name, PList const &gapl); // opening
protected:                                                           // for File construction
    Group(hid_t handle, deleter_type deleter, std::string path)
    : Object{ handle, deleter }, m_path{ std::move(path) }, is_file{ true }
    {
    }

public:
    /// @name group attributes
    /// @{

    /// Path of `*this` in HDF5 tree.
    ///
    [[nodiscard]] std::string path() const { return m_path; }

    /// Creation PList of `*this`.
    ///
    [[nodiscard]] PList cpl() const;

    /// Flush `*this` out to disk.
    /// @details Local file scope flush.
    ///
    void flush();
    /// @}

    /// @name subgroup access/creation
    /// @{

    /// Create a named group under `*this`.
    ///
    [[nodiscard]] auto group(char const *name, PList const &gapl, PList const &gcpl,
                             PList const &lcpl = PList{})
    {
        return Group{ *this, name, gapl, gcpl, lcpl };
    }

    /// Open a named group under `*this`.
    ///
    [[nodiscard]] auto group(char const *name, PList const &gapl = PList{}) const
    {
        return Group{ *this, name, gapl };
    }
    /// @}

    /// @name attribute access/creation
    /// @{

    /// Create a named attribute under `*this`.
    ///
    [[nodiscard]] auto attribute(char const *name, Type const &type, Space const &space,
                                 PList const &aapl = PList{}, PList const &acpl = PList{})
    {
        return Attribute{ *this, name, type, space, aapl, acpl };
    }

    /// Open a named attribute under `*this`.
    ///
    [[nodiscard]] auto attribute(char const *name, PList const &aapl = PList{}) const
    {
        return Attribute{ *this, name, aapl };
    }
    /// @}

    /// @name dataset access/creation
    /// @{

    /// Create a named dataset under `*this`.
    ///
    [[nodiscard]] auto dataset(char const *name, Type const &type, Space const &space,
                               PList const &dapl = PList{}, PList const &dcpl = PList{},
                               PList const &lcpl = PList{})
    {
        return Dataset{ *this, name, type, space, dapl, dcpl, lcpl };
    }

    /// Open a named dataset under `*this`.
    ///
    [[nodiscard]] auto dataset(char const *name, PList const &dapl = PList{}) const
    {
        return Dataset{ *this, name, dapl };
    }
    /// @}
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5Group_h */

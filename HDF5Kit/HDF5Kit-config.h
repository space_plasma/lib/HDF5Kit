/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/// @file
/// Defines library-level config macros.
///

/// @def HDF5KIT_ROOT_NAMESPACE
/// API root namespace macro definition
///
/// All non-global, public APIs are under this root namespace.
///

/// @def HDF5KIT_VERSION_NAMESPACE(ver)
/// API versioning namespace macro definition
///
/// The version namespace is formed by concatenating of `v_`, ver, and `_`.
/// @param ver The version number.
///

/// @def HDF5KIT_NAMESPACE(ver)
/// The namespace under which the specific version of APIs is defined
///
/// The APIs of the given version are defined under
/// HDF5KIT_ROOT_NAMESPACE::HDF5KIT_VERSION_NAMESPACE(ver).
/// @param ver The version number.
///

#ifndef HDF5Kit_config_h
#define HDF5Kit_config_h

/// @cond
// macro operation helpers
// https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
//
#define _HDF5KIT_CAT(first, ...)           _HDF5KIT_PRIMITIVE_CAT(first, __VA_ARGS__)
#define _HDF5KIT_PRIMITIVE_CAT(first, ...) first##__VA_ARGS__

#define _HDF5KIT_CHECK_N(x, n, ...) n
#define _HDF5KIT_CHECK(...)         _HDF5KIT_CHECK_N(__VA_ARGS__, 0, )
#define _HDF5KIT_PROBE(x)           x, 1,

#define _HDF5KIT_PAIR_1_1      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_2_2      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_3_3      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_4_4      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_5_5      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_6_6      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_7_7      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_8_8      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR_9_9      _HDF5KIT_PROBE(~)
#define _HDF5KIT_PAIR(x, y)    _HDF5KIT_CAT(_HDF5KIT_CAT(_HDF5KIT_CAT(_HDF5KIT_PAIR_, x), _), y)
#define _HDF5KIT_IS_SAME(x, y) _HDF5KIT_CHECK(_HDF5KIT_PAIR(x, y))
/// @endcond

// namespace
//
#ifndef HDF5KIT_ROOT_NAMESPACE
#define HDF5KIT_ROOT_NAMESPACE hdf5
/// @cond
#define _HDF5KIT_NAMESPACE_ROOT namespace HDF5KIT_ROOT_NAMESPACE
/// @endcond
#endif

#ifndef HDF5KIT_VERSION_NAMESPACE
#define HDF5KIT_VERSION_NAMESPACE(ver) _HDF5KIT_CAT(_HDF5KIT_CAT(v_, ver), _)
/// @cond
#define _HDF5KIT_NAMESPACE_VERSION_0(ver) namespace HDF5KIT_VERSION_NAMESPACE(ver)
#define _HDF5KIT_NAMESPACE_VERSION_1(ver) inline namespace HDF5KIT_VERSION_NAMESPACE(ver)
#define _HDF5KIT_NAMESPACE_VERSION(ver)                                                            \
    _HDF5KIT_CAT(_HDF5KIT_NAMESPACE_VERSION_, _HDF5KIT_IS_SAME(ver, HDF5KIT_INLINE_VERSION))(ver)
/// @endcond
#endif

#ifndef HDF5KIT_NAMESPACE
#define HDF5KIT_NAMESPACE(ver) HDF5KIT_ROOT_NAMESPACE::HDF5KIT_VERSION_NAMESPACE(ver)
/// @cond
#define HDF5KIT_NAMESPACE_BEGIN(ver)                                                               \
    _HDF5KIT_NAMESPACE_ROOT                                                                        \
    {                                                                                              \
        _HDF5KIT_NAMESPACE_VERSION(ver)                                                            \
        {
//
#define HDF5KIT_NAMESPACE_END(ver)                                                                 \
    }                                                                                              \
    }
/// @endcond
#endif

#endif /* HDF5Kit_config_h */

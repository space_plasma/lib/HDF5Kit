/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5Dataset.h"
#include "H5Group.h"
#include "H5Space.h"
#include "H5Type.h"

#include <stdexcept>

HDF5KIT_NAMESPACE_BEGIN(1)

Dataset::Dataset(decltype(nullptr), hid_t handle) : Object{ handle, &H5Dclose }
{
}
Dataset::Dataset(Group const &parent, char const *name, Type const &type, Space const &space,
                 PList const &dapl, PList const &dcpl, PList const &lcpl)
: Dataset{ nullptr, H5Dcreate2(*parent, name, *type, *space, *lcpl, *dcpl, *dapl) }
{
    m_path = parent.path();
    if ('/' == m_path.back()) {
        m_path.pop_back();
    }
    m_path = m_path + '/' + name;
}
Dataset::Dataset(Group const &parent, char const *name, PList const &dapl)
: Dataset{ nullptr, H5Dopen2(*parent, name, *dapl) }
{
    m_path = parent.path();
    if ('/' == m_path.back()) {
        m_path.pop_back();
    }
    m_path = m_path + "/" + name;
}

auto Dataset::type() const -> Type
{
    if (hid_t id = H5Dget_type(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dget_type returned error" };
    } else
        return { nullptr, id };
}
auto Dataset::cpl() const -> PList
{
    if (hid_t id = H5Dget_create_plist(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dget_create_plist returned error" };
    } else
        return { nullptr, id };
}
auto Dataset::apl() const -> PList
{
    if (hid_t id = H5Dget_access_plist(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dget_access_plist returned error" };
    } else
        return { nullptr, id };
}
auto Dataset::space() const -> Space
{
    if (hid_t id = H5Dget_space(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dget_space returned error" };
    } else
        return { nullptr, id };
}
void Dataset::set_extent(Extent const &dims)
{
    if (static_cast<long>(dims.rank()) < space().rank())
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - array size less than dimension rank" };

    if (H5Dset_extent(**this, dims.data()) < 0)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dset_extent returned error" };
}

void Dataset::flush()
{
    if (H5Fflush(**this, H5F_SCOPE_LOCAL) < 0)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Fflush returned error" };
}

void Dataset::write(Space const &fspace, void const *data, Type const &type, Space const &mspace,
                    PList const &xfer)
{
    if (H5Dwrite(**this, *type, *mspace, *fspace, *xfer, data) < 0)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dwrite returned error" };
}
void Dataset::read(Space const &fspace, void *buffer, Type const &type, Space const &mspace,
                   PList const &xfer) const
{
    if (H5Dread(**this, *type, *mspace, *fspace, *xfer, buffer) < 0)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Dread returned error" };
}

HDF5KIT_NAMESPACE_END(1)

/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5PList.h"

#include <stdexcept>
#include <string>

HDF5KIT_NAMESPACE_BEGIN(1)

PList::PList(decltype(nullptr), hid_t handle) : Object{ handle, &H5Pclose }
{
}
PList::PList(PList const &o) : Object{}
{
    if (o) { // protect against copying invalid object
        *this = { nullptr, H5Pcopy(*o) };
    }
}

PList::PList(hid_t plist_class) : PList{ nullptr, H5Pcreate(plist_class) }
{
}

void PList::set_chunk(Extent const &dims)
{
    if (H5Pset_chunk(**this, static_cast<int>(dims.rank()), dims.data()) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Pset_chunk returned error" };
    }
}
auto PList::chunk() const -> Extent
{
    Extent dims;
    if (int const rank = H5Pget_chunk(**this, Extent::max_size(), dims.data()); rank >= 0) {
        dims.m_rank = static_cast<unsigned>(rank);
        return dims;
    } else
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Pget_chunk returned error" };
}

HDF5KIT_NAMESPACE_END(1)

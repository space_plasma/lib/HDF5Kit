/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_TypeMap_h
#define HDF5Kit_TypeMap_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/Extent.h>
#include <HDF5Kit/H5Type.h>

#include <array>
#include <complex>
#include <type_traits>
#include <utility>

HDF5KIT_NAMESPACE_BEGIN(1)

/// Adapter for Type construction
/// @details For a given type `T`, requirements for TypeMap<T> are:
///
/// - Make `TypeMap<T>' class be default-constructible;
/// - Define a call operator with no argument and that returns the Type instance corresponding to
/// `T`;
/// - All subsequent call to the call operator must return a Type instance whose typemap is
/// equivalent to that of the first call (that is, any operations using all Type instances returned
/// from this call are expected to behave the same as using any of the Type instances);
/// - Optionally, declare type alias `type = T`.
///
template <class T> struct TypeMap;

// MARK:- Type Maker
/// @name Type Maker
/// @{

/// Create a Type object corresponding to `T`.
///
template <class T> [[nodiscard]] auto make_type()
{
    return TypeMap<std::decay_t<T>>{}();
}
/// Create a Type object corresponding to `T`.
/// @details The argument is for automatic type deduction and will be untouched.
///
template <class T> [[nodiscard]] auto make_type(T &&)
{
    return make_type<T>();
}
/// @}

// MARK:- Native Types
//
/// @name TypeMap for Native Types
/// @{

/// TypeMap for `char`
///
template <> struct TypeMap<char> {
    using type = char;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<char>(); }
};
/// TypeMap for `unsigned char`
///
template <> struct TypeMap<unsigned char> {
    using type = unsigned char;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<unsigned char>(); }
};

/// TypeMap for `short`
///
template <> struct TypeMap<short> {
    using type = short;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<short>(); }
};
/// TypeMap for `unsigned short`
///
template <> struct TypeMap<unsigned short> {
    using type = unsigned short;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<unsigned short>(); }
};

/// TypeMap for `int`
///
template <> struct TypeMap<int> {
    using type = int;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<int>(); }
};
/// TypeMap for `unsigned int`
///
template <> struct TypeMap<unsigned> {
    using type = unsigned;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<unsigned>(); }
};

/// TypeMap for `long`
///
template <> struct TypeMap<long> {
    using type = long;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<long>(); }
};
/// TypeMap for `unsigned long`
///
template <> struct TypeMap<unsigned long> {
    using type = unsigned long;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<unsigned long>(); }
};

/// TypeMap for `float`
///
template <> struct TypeMap<float> {
    using type = float;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<float>(); }
};
/// TypeMap for `double`
///
template <> struct TypeMap<double> {
    using type = double;
    [[nodiscard]] Type operator()() const noexcept { return Type::Native<double>(); }
};
/// @}

// MARK:- stdlib Types
//
/// @name TypeMap for stdlib Types
/// @{

/// TypeMap for `std::complex`
///
template <class T> struct TypeMap<std::complex<T>> {
    using type = std::complex<T>;
    [[nodiscard]] Type operator()() const { return make_type<T>().array(2); }
};

/// TypeMap for `std::pair`
///
template <class T, class U> struct TypeMap<std::pair<T, U>> {
    using type = std::pair<T, U>;
    [[nodiscard]] Type operator()() const
    {
        if constexpr (std::is_same_v<T, U>) {
            return make_type<T>().array(2);
        } else {
            auto t = Type::compound(sizeof(type));
            t.insert("first", HOFFSET(type, first),
                     make_type<decltype(std::declval<type>().first)>());
            t.insert("second", HOFFSET(type, second),
                     make_type<decltype(std::declval<type>().second)>());
            return t;
        }
    }
};

/// TypeMap for `std::array`
///
template <class T, unsigned long N> struct TypeMap<std::array<T, N>> {
    using type = std::array<T, N>;
    [[nodiscard]] Type operator()() const { return make_type<T>().array(N); }
};
/// @}

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_TypeMap_h */

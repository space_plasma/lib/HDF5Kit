/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5Type_h
#define HDF5Kit_H5Type_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/Extent.h>
#include <HDF5Kit/H5Object.h>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class Attribute;
class Dataset;
class PList;

/// Wrapper for HDF5 data type.
///
class Type : public Object {
    friend Attribute;
    friend Dataset;

    Type(decltype(nullptr), hid_t handle);
    // helper to construct Type object of the given class.
    Type(H5T_class_t type_class, unsigned long size);

public:
    /// @{
    Type() noexcept        = default;
    Type(Type &&) noexcept = default;
    Type &operator=(Type &&) noexcept = default;
    Type(Type const &other);
    Type &operator=(Type const &other) { return *this = Type{ other }; }
    /// @}

public: // general datatype operations
    /// @name General Datatype Operations
    /// @{

    /// Determines whether a datatype is a committed or a transient type.
    ///
    [[nodiscard]] bool is_committed() const;

    /// Returns the datatype class identifier.
    ///
    [[nodiscard]] H5T_class_t class_() const;

    /// Returns a copy of a datatype creation property list.
    ///
    [[nodiscard]] PList cpl() const;

    /// Returns the size of a datatype.
    ///
    [[nodiscard]] unsigned size() const;

    /// Returns the base datatype from which a datatype is derived.
    ///
    [[nodiscard]] Type super_() const;

    /// Returns the native datatype of a specified datatype.
    ///
    [[nodiscard]] Type native(H5T_direction_t) const;

    /// Returns the byte order of a datatype.
    ///
    [[nodiscard]] H5T_order_t order() const;
    /// @}

public: // type construction
    /// @name Type Construction
    /// @{

    /// Native type factory
    /// @tparam T One of the native types.
    ///
    template <class T> class Native;

    /// Create a compound Type object of the given size.
    ///
    [[nodiscard]] static auto compound(unsigned long size) { return Type{ H5T_COMPOUND, size }; }

    /// Insert a field to a compound Type object.
    /// @param name Field name.
    /// @param offset Offset of the field, obtained from, e.g., `HOFFSET(A_struct, a_field)`.
    /// @param type A Type object corresponding to the field.
    ///
    void insert(char const *name, unsigned long offset, Type const &type);

    /// Create an opaque Type object of the given size.
    ///
    [[nodiscard]] static auto opaque(unsigned long size) { return Type{ H5T_OPAQUE, size }; }

    /// Create an enumeration Type object with `*this` as its base type.
    ///
    [[nodiscard]] Type enumeration() const;

    /// Create a variable-length Type object with `*this` as its base type.
    ///
    [[nodiscard]] Type vlen() const;

    /// Create an array Type object of the given dimension sizes.
    /// @exception Rank of the Extent must not be zero.
    ///
    [[nodiscard]] Type array(Extent const &dims) const;
    /// @}

protected:
    using Object::Object; // for native type construction
};

// native type subclasses
//
#include <HDF5Kit/NativeType.hh>

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5Type_h */

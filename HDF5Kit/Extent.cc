/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "Extent.h"
#include <type_traits>

HDF5KIT_NAMESPACE_BEGIN(1)

auto Extent::at(size_type const i) -> reference
{
    static_assert(std::is_unsigned_v<decltype(i)>);
    if (i < m_rank) {
        return dims[i];
    }
    throw std::out_of_range{ __PRETTY_FUNCTION__ };
}
auto Extent::at(size_type const i) const -> const_reference
{
    static_assert(std::is_unsigned_v<decltype(i)>);
    if (i < m_rank) {
        return dims[i];
    }
    throw std::out_of_range{ __PRETTY_FUNCTION__ };
}

HDF5KIT_NAMESPACE_END(1)

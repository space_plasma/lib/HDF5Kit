/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5File.h"
#include "H5PList.h"

#include <memory>
#include <stdexcept>

HDF5KIT_NAMESPACE_BEGIN(1)

File::trunc_tag::trunc_tag() noexcept : create_tag{ H5F_ACC_TRUNC }
{
}
File::excl_tag::excl_tag() noexcept : create_tag{ H5F_ACC_EXCL }
{
}
File::rdwr_tag::rdwr_tag() noexcept : open_tag{ H5F_ACC_RDWR }
{
}
File::rdonly_tag::rdonly_tag() noexcept : open_tag{ H5F_ACC_RDONLY }
{
}
// clang-format off
File::File(create_tag const &tag, char const *filepath, PList const &fapl, PList const &fcpl)
try : Group{H5Fcreate(filepath, tag.flag, *fcpl, *fapl), &H5Fclose, "/"} {
} catch (std::exception const &e) {
    throw std::runtime_error{std::string{__PRETTY_FUNCTION__} + " - " + e.what()};
}
File::File(open_tag const &tag, char const *filepath, PList const &fapl)
try : Group{H5Fopen(filepath, tag.flag, *fapl), &H5Fclose, "/"} {
} catch (std::exception const &e) {
    throw std::runtime_error{std::string{__PRETTY_FUNCTION__} + " - " + e.what()};
}
// clang-format on
auto File::file_path() const -> std::string
{
    // determine length
    //
    auto const _len = H5Fget_name(**this, nullptr, 0);
    if (_len < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - failed to determine file length" };
    }
    auto const len  = static_cast<unsigned long>(_len + 1);
    auto       path = std::make_unique<char[]>(len);

    // retrieve file path
    //
    if (H5Fget_name(**this, path.get(), len) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - failed to retrieve file path" };
    }
    return path.get();
}
auto File::cpl() const -> PList
{
    if (hid_t id = H5Fget_create_plist(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Fget_create_plist returned error" };
    } else
        return { nullptr, id };
}
auto File::apl() const -> PList
{
    if (hid_t id = H5Fget_access_plist(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Fget_access_plist returned error" };
    } else
        return { nullptr, id };
}

HDF5KIT_NAMESPACE_END(1)

/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_Extent_h
#define HDF5Kit_Extent_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <algorithm>
#include <array>
#include <iterator>
#include <ostream>
#include <stdexcept>
#include <type_traits>

#include <HDF5Kit/hdf5_wrapper.h>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class PList;
class Space;

/// Abstraction for dimension extent.
///
class Extent {
    friend Space;
    friend PList;
    using array_t = std::array<hsize_t, H5S_MAX_RANK>;

    unsigned m_rank{};
    array_t  dims{};

public:
    constexpr explicit Extent() noexcept = default;

    /// Construct an Extent object from the contents pointed to by the iterators
    /// @exception Rank is determined from the iterator distance which must be less than or equal to
    /// `H5S_MAX_RANK`.
    /// @tparam It Forward iterator whose value type should be convertible to `hsize_t`.
    /// @param first Beginning of the iterator.
    /// @param last End of the iterator.
    ///
    template <class It, std::enable_if_t<
                            std::is_base_of_v<std::forward_iterator_tag,
                                              typename std::iterator_traits<It>::iterator_category>,
                            int> = 0>
    Extent(It first, It last)
    {
        static_assert(std::is_convertible_v<typename std::iterator_traits<It>::value_type, hsize_t>,
                      "values pointed to by iterator not convertible to hsize_t");
        auto const rank = std::distance(first, last);
        if (rank >= 0 && rank < static_cast<long>(std::tuple_size_v<array_t>)) {
            m_rank = static_cast<unsigned>(rank);
            std::copy(first, last, dims.data());
        } else {
            throw std::invalid_argument{ __PRETTY_FUNCTION__ };
        }
    }

    /// Create one-dimensional Extent.
    constexpr Extent(hsize_t Nx) noexcept : m_rank{ 1 }, dims{ Nx } {}
    /// Create two-dimensional Extent.
    constexpr Extent(hsize_t Nx, hsize_t Ny) noexcept : m_rank{ 2 }, dims{ Nx, Ny } {}
    /// Create three-dimensional Extent.
    constexpr Extent(hsize_t Nx, hsize_t Ny, hsize_t Nz) noexcept : m_rank{ 3 }, dims{ Nx, Ny, Nz }
    {
    }
    /// Create four-dimensional Extent.
    constexpr Extent(hsize_t Nx, hsize_t Ny, hsize_t Nz, hsize_t Nu) noexcept
    : m_rank{ 4 }, dims{ Nx, Ny, Nz, Nu }
    {
    }
    /// Create five-dimensional Extent.
    constexpr Extent(hsize_t Nx, hsize_t Ny, hsize_t Nz, hsize_t Nu, hsize_t Nv) noexcept
    : m_rank{ 5 }, dims{ Nx, Ny, Nz, Nu, Nv }
    {
    }
    /// Create six-dimensional Extent.
    constexpr Extent(hsize_t Nx, hsize_t Ny, hsize_t Nz, hsize_t Nu, hsize_t Nv,
                     hsize_t Nw) noexcept
    : m_rank{ 6 }, dims{ Nx, Ny, Nz, Nu, Nv, Nw }
    {
    }

    /// Create an Extent object of the given rank with sizes set to `H5S_UNLIMITED`
    /// @exception If the given rank is larger than `H5S_MAX_RANK`.
    ///
    [[nodiscard]] static auto unlimited(unsigned rank)
    {
        Extent dims;
        if (rank > std::tuple_size_v<array_t>) {
            throw std::invalid_argument{ __PRETTY_FUNCTION__ };
        }
        dims.m_rank = rank;
        std::fill(dims.begin(), dims.end(), H5S_UNLIMITED);
        return dims;
    }

public:
    /// @name Typedefs
    /// @{
    using value_type             = hsize_t;
    using reference              = std::add_lvalue_reference_t<value_type>;
    using const_reference        = std::add_lvalue_reference_t<value_type const>;
    using pointer                = std::add_pointer_t<value_type>;
    using const_pointer          = std::add_pointer_t<value_type const>;
    using iterator               = pointer;
    using const_iterator         = const_pointer;
    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using size_type              = array_t::size_type;
    using difference_type        = array_t::difference_type;
    /// @}

public:
    /// @name Capacity
    /// @{
    [[nodiscard]] constexpr unsigned         rank() const noexcept { return m_rank; }
    [[nodiscard]] constexpr size_type        size() const noexcept { return rank(); }
    [[nodiscard]] static constexpr size_type max_size() noexcept
    {
        return std::tuple_size_v<array_t>;
    }
    [[nodiscard]] constexpr bool empty() const noexcept { return !m_rank; }
    /// @}

public:
    /// @name Iterators
    /// @{
    [[nodiscard]] constexpr iterator       begin() noexcept { return dims.data(); }
    [[nodiscard]] constexpr const_iterator begin() const noexcept { return dims.data(); }
    [[nodiscard]] constexpr const_iterator cbegin() const noexcept { return begin(); }

    [[nodiscard]] constexpr iterator       end() noexcept { return begin() + m_rank; }
    [[nodiscard]] constexpr const_iterator end() const noexcept { return begin() + m_rank; }
    [[nodiscard]] constexpr const_iterator cend() const noexcept { return end(); }

    [[nodiscard]] constexpr auto rbegin() noexcept { return reverse_iterator{ end() }; }
    [[nodiscard]] constexpr auto rbegin() const noexcept { return const_reverse_iterator{ end() }; }
    [[nodiscard]] constexpr auto crbegin() const noexcept { return rbegin(); }

    [[nodiscard]] constexpr auto rend() noexcept { return reverse_iterator{ begin() }; }
    [[nodiscard]] constexpr auto rend() const noexcept { return const_reverse_iterator{ begin() }; }
    [[nodiscard]] constexpr auto crend() const noexcept { return rend(); }
    /// @}

public:
    /// @name Modifiers
    /// @{
    void fill(value_type v) noexcept { std::fill_n(dims.data(), m_rank, v); }

    void swap(Extent &other) noexcept
    {
        using std::swap;
        std::swap_ranges(other.begin(), other.end(), this->begin());
    }
    friend void swap(Extent &a, Extent &b) noexcept { a.swap(b); }
    /// @}

public:
    /// @name Accessors
    /// @{
    [[nodiscard]] constexpr auto data() noexcept { return dims.data(); }
    [[nodiscard]] constexpr auto data() const noexcept { return dims.data(); }
    //
    [[nodiscard]] constexpr decltype(auto) front() noexcept { return *begin(); }
    [[nodiscard]] constexpr decltype(auto) front() const noexcept { return *begin(); }
    //
    [[nodiscard]] constexpr decltype(auto) back() noexcept { return *rbegin(); }
    [[nodiscard]] constexpr decltype(auto) back() const noexcept { return *rbegin(); }
    //
    [[nodiscard]] auto at(size_type i) -> reference;
    [[nodiscard]] auto at(size_type i) const -> const_reference;
    //
#if defined(DEBUG)
    [[nodiscard]] decltype(auto) operator[](size_type const i) { return at(i); }
    [[nodiscard]] decltype(auto) operator[](size_type const i) const { return at(i); }
#else
    [[nodiscard]] constexpr decltype(auto) operator[](size_type const i) noexcept
    {
        return dims[i];
    }
    [[nodiscard]] constexpr decltype(auto) operator[](size_type const i) const noexcept
    {
        return dims[i];
    }
#endif
    /// @}

private: // pretty print
    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, Extent const &dims)
    {
        if (unsigned const rank = dims.rank()) {
            os << '{' << dims.front();
            for (unsigned i = 1; i < rank; ++i) {
                os << ", " << dims[i];
            }
            return os << '}';
        } else {
            return os << "{}";
        }
    }
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_Extent_h */

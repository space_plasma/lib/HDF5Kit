/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#if defined(__APPLE__) && defined(__OBJC__)
#import <Cocoa/Cocoa.h>

//! Project version number for HDF5Kit.
FOUNDATION_EXPORT double HDF5KitVersionNumber;

//! Project version string for HDF5Kit.
FOUNDATION_EXPORT const unsigned char HDF5KitVersionString[];
#endif

// In this header, you should import all the public headers of your framework using statements like
// #import <HDF5Kit/PublicHeader.h>

/// @file
/// `HDF5Kit` umbrella header.
///
/// The whole library API can be included using
///
/// > #include <HDF5Kit/HDF5Kit.h>
///

/// @namespace hdf5
/// Namespace for HDF5Kit.
///

/// @def HDF5KIT_INLINE_VERSION
/// Defines the version namespace to be inlined
///
/// Defining this macro with the version number to be inlined before including the library headers
/// enables inlining the specified version namespace.
/// @see HDF5Kit/HDF5Kit-config.h.
///
#if !defined(HDF5KIT_INLINE_VERSION)
// default version selection
//
#define HDF5KIT_INLINE_VERSION 1
#endif

#if defined(__cplusplus)

#include <HDF5Kit/Extent.h>
#include <HDF5Kit/H5Attribute.h>
#include <HDF5Kit/H5Dataset.h>
#include <HDF5Kit/H5File.h>
#include <HDF5Kit/H5Group.h>
#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5PList.h>
#include <HDF5Kit/H5Space.h>
#include <HDF5Kit/H5Type.h>
#include <HDF5Kit/TypeMap.h>

#endif

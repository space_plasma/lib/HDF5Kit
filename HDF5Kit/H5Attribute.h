/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5Attribute_h
#define HDF5Kit_H5Attribute_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/TypeMap.h>

#include <string>
#include <type_traits>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class Dataset;
class Group;
class Space;
class PList;
class Type;

/**
 @brief Wrapper for HDF5 attribute.
 */
class Attribute final : public Object {
    friend Dataset;
    friend Group;

    Attribute(decltype(nullptr), hid_t handle);

public:
    /// @{
    Attribute() noexcept             = default;
    Attribute(Attribute &&) noexcept = default;
    Attribute &operator=(Attribute &&) noexcept = default;
    /// @}
private: // to be used by Groud and Dataset
    Attribute(Object const &parent, char const *name, Type const &type, Space const &space,
              PList const &aapl, PList const &acpl);                      // creation
    Attribute(Object const &parent, char const *name, PList const &aapl); // opening

public:
    /// @name Query
    /// @{

    /// Name of `*this`.
    ///
    [[nodiscard]] std::string name() const;

    /// Data Space of `*this`.
    ///
    [[nodiscard]] Space space() const;

    /// Data Type of `*this`.
    ///
    [[nodiscard]] Type type() const;

    /// Creation PList of `*this`.
    ///
    [[nodiscard]] PList cpl() const;
    /// @}

public:
    /// @name Read/write operations
    /// @{

    /// Flush the content of `*this` out to disk.
    /// @details Local file scope flush.
    ///
    void flush();

    /// Write out data of the given Type.
    ///
    void write(void const *data, Type const &type);

    /// Write out a value of type T.
    ///
    template <class T> void write(T const &v)
    {
        write(&v, make_type<T>());
        static_assert(std::is_default_constructible_v<TypeMap<T>>, "TypeMap is not defined");
    }

    /// Read in data of the given Type.
    ///
    void read(void *buffer, Type const &type) const;

    /// Read in a value of type T.
    ///
    template <class T> void read(T &v) const
    {
        read(&v, make_type<T>());
        static_assert(std::is_default_constructible_v<TypeMap<T>>, "TypeMap is not defined");
    }

    /// Read in a value of type T.
    ///
    template <class T> [[nodiscard]] auto read() const
    {
        std::decay_t<T> ret;
        read(ret);
        return ret; // NRVO
        static_assert(std::is_default_constructible_v<decltype(ret)>);
    }
    /// @}
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5Attribute_h */

/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_NativeType_hh
#define HDF5Kit_NativeType_hh

/// @cond
#define HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(type)                                           \
    template <> class Type::Native<type> final : public Type {                                     \
    public:                                                                                        \
        Native() noexcept;                                                                         \
        Native(Native &&) noexcept = default;                                                      \
        Native &operator=(Native &&) noexcept = default;                                           \
    }

HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(char);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned char);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(short);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned short);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(int);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(long);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned long);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(float);
HDF5KIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(double);
/// @endcond

#endif /* HDF5Kit_NativeType_hh */

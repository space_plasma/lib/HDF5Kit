/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5Space.h"

#include <stdexcept>
#include <string>

HDF5KIT_NAMESPACE_BEGIN(1)

Space::Space(decltype(nullptr), hid_t handle) : Object{ handle, &H5Sclose }
{
}
Space::Space(Space const &o) : Object{}
{
    if (o) { // protect against copying invalid object
        *this = { nullptr, H5Scopy(*o) };
    }
}

Space::Space(H5S_class_t const space_class) : Space{ nullptr, H5Screate(space_class) }
{
}
auto Space::simple(Extent const &dims, Extent const *max_dims) -> Space
{
    if (max_dims && max_dims->rank() < dims.rank()) {
        throw std::invalid_argument{
            std::string{ __PRETTY_FUNCTION__ }
            + " - rank of max_dims must be greater than or equal to that of dims"
        };
    }
    return { nullptr, H5Screate_simple(static_cast<int>(dims.rank()), dims.data(),
                                       (max_dims ? max_dims->data() : nullptr)) };
}
bool Space::is_simple() const
{
    if (htri_t const simple = H5Sis_simple(**this); simple >= 0) {
        return simple;
    }
    throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                             + " - H5Sis_simple returned error" };
}
auto Space::rank() const -> unsigned
{
    if (int const rank = H5Sget_simple_extent_ndims(**this); rank >= 0) {
        return static_cast<unsigned>(rank);
    }
    throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                             + " - H5Sget_simple_extent_ndims returned error" };
}
auto Space::simple_extent() const -> std::pair<Extent, Extent>
{
    std::pair<Extent, Extent> dims;
    if (int rank = H5Sget_simple_extent_dims(**this, dims.first.data(), dims.second.data());
        rank < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Sget_simple_extent_dims returned error" };
    } else {
        dims.second.m_rank = dims.first.m_rank = static_cast<unsigned>(rank);
    }
    return dims;
}

void Space::select_all()
{
    if (H5Sselect_all(**this) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Sselect_all returned error" };
    }
}
void Space::select_none()
{
    if (H5Sselect_none(**this) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Sselect_none returned error" };
    }
}
void Space::select(H5S_seloper_t sel_op, Extent const &start, Extent const &count,
                   Extent const *stride, Extent const *block)
{
    // verify array sizes
    //
    long const rank = this->rank();
    if (start.rank() < rank || count.rank() < rank || (stride && stride->rank() < rank)
        || (block && block->rank() < rank)) {
        throw std::invalid_argument{
            std::string{ __PRETTY_FUNCTION__ }
            + " - one or more of selection array sizes less than dimension rank"
        };
    }

    // select
    //
    if (H5Sselect_hyperslab(**this, sel_op, start.data(), (stride ? stride->data() : nullptr),
                            count.data(), (block ? block->data() : nullptr))
        < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Sselect_hyperslab returned error" };
    }
}

HDF5KIT_NAMESPACE_END(1)

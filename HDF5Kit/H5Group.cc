/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5Group.h"
#include "H5File.h"

#include <stdexcept>

HDF5KIT_NAMESPACE_BEGIN(1)

Group::Group(decltype(nullptr), hid_t handle) : Object{ handle, &H5Gclose }
{
}
Group::Group(Group const &parent, char const *name, PList const &gapl, PList const &gcpl,
             PList const &lcpl)
: Group{ nullptr, H5Gcreate2(*parent, name, *lcpl, *gcpl, *gapl) }
{
    m_path = parent.path();
    if ('/' == m_path.back()) {
        m_path.pop_back();
    }
    m_path = m_path + "/" + name;
}
Group::Group(Group const &parent, char const *name, PList const &gapl)
: Group{ nullptr, H5Gopen2(*parent, name, *gapl) }
{
    m_path = parent.path();
    if ('/' == m_path.back()) {
        m_path.pop_back();
    }
    m_path = m_path + "/" + name;
}

auto Group::cpl() const -> PList
{
    if (is_file) {
        return static_cast<File const *>(this)->cpl();
    }
    if (hid_t id = H5Gget_create_plist(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Gget_create_plist returned error" };
    } else
        return { nullptr, id };
}

void Group::flush()
{
    if (H5Fflush(**this, H5F_SCOPE_LOCAL) < 0)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Fflush returned error" };
}

HDF5KIT_NAMESPACE_END(1)

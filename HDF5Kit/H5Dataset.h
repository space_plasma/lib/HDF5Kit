/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5Dataset_h
#define HDF5Kit_H5Dataset_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/Extent.h>
#include <HDF5Kit/H5Attribute.h>
#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5PList.h>
#include <HDF5Kit/TypeMap.h>

#include <string>
#include <type_traits>
#include <utility>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class Group;
class Space;
class Type;

/// Wrapper for HDF5 attribute.
///
class Dataset final : public Object {
    friend Group;
    std::string m_path{};

    Dataset(decltype(nullptr), hid_t handle);

public:
    /// @{
    Dataset() noexcept           = default;
    Dataset(Dataset &&) noexcept = default;
    Dataset &operator=(Dataset &&) noexcept = default;
    /// @}
private: // to be used by Groud
    Dataset(Group const &parent, char const *name, Type const &type, Space const &space,
            PList const &dapl, PList const &dcpl, PList const &lcpl);  // creation
    Dataset(Group const &parent, char const *name, PList const &dapl); // opening

public:
    /// @name Dataset Attributes
    /// @{

    /// Path of `*this` in HDF5 tree.
    ///
    [[nodiscard]] std::string path() const { return m_path; }

    /// Data Type of `*this`.
    ///
    [[nodiscard]] Type type() const;

    /// Creation PList of `*this`.
    ///
    [[nodiscard]] PList cpl() const;

    /// Access PList of `*this`.
    ///
    [[nodiscard]] PList apl() const;

    /// Data Space of `*this`.
    ///
    [[nodiscard]] Space space() const;

    /// Set Extent of `*this`.
    ///
    void set_extent(Extent const &dims);
    /// @}

public:
    /// @name Attribute Access/Creation
    /// @{

    /// Create a named Attribute under `*this`.
    ///
    auto attribute(char const *name, Type const &type, Space const &space,
                   PList const &aapl = PList{}, PList const &acpl = PList{})
    {
        return Attribute{ *this, name, type, space, aapl, acpl };
    }

    /// Open a named Attribute under `*this`.
    ///
    auto attribute(char const *name, PList const &aapl = PList{}) const
    {
        return Attribute{ *this, name, aapl };
    }
    /// @}

public:
    /// @name Read/Write Operations
    /// @{

    /// Flush `*this` out to disk.
    /// @details Local file scope flush.
    ///
    void flush();

    /// Write out data of the given Type to the given Space.
    ///
    void write(Space const &fspace, void const *data, Type const &type, Space const &mspace,
               PList const &xfer = PList{});

    /// Write out data of type T to the given Space.
    ///
    template <class T>
    void write(Space const &fspace, T const *data, Space const &mspace, PList const &xfer = PList{})
    {
        write(fspace, data, make_type<T>(), mspace, xfer);
        static_assert(std::is_default_constructible_v<TypeMap<T>>, "TypeMap is not defined");
    }

    /// Read in data of the given Type from the given Space.
    ///
    void read(Space const &fspace, void *buffer, Type const &type, Space const &mspace,
              PList const &xfer = PList{}) const;

    /// Read in data of type T from the given Space.
    ///
    template <class T>
    void read(Space const &fspace, T *buffer, Space const &mspace,
              PList const &xfer = PList{}) const
    {
        read(fspace, buffer, make_type<T>(), mspace, xfer);
        static_assert(std::is_default_constructible_v<TypeMap<T>>, "TypeMap is not defined");
    }
    /// @}
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5Dataset_h */

/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5Space_h
#define HDF5Kit_H5Space_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/Extent.h>
#include <HDF5Kit/H5Object.h>

#include <utility>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class Attribute;
class Dataset;

/// Wrapper for HDF5 dataspace.
///
class Space final : public Object {
    friend Attribute;
    friend Dataset;

    Space(decltype(nullptr), hid_t handle);

public:
    /// @{
    Space() noexcept         = default;
    Space(Space &&) noexcept = default;
    Space &operator=(Space &&) noexcept = default;
    Space(Space const &other);
    Space &operator=(Space const &other) { return *this = Space{ other }; }
    /// @}

    /// Construct Space object of the given class.
    ///
    explicit Space(H5S_class_t space_class);

public:
    /// @name Query
    /// @{

    /// Determines whether `*this` is a simple dataspace.
    ///
    [[nodiscard]] bool is_simple() const;

    /// Get rank of simple dataspace.
    ///
    [[nodiscard]] unsigned rank() const;

    /// Get dimension sizes and maximum dimension sizes of simple dataspace.
    /// @return A pair of Extent objects.
    ///
    [[nodiscard]] auto simple_extent() const -> std::pair<Extent, Extent>;
    /// @}

public:
    /// @name Select operations
    /// @{

    /// Select all.
    ///
    void select_all();

    /// Select none.
    ///
    void select_none();

    /// Select hyperslab space.
    /// @exception Ranks of the Extent objects must be greater than or equal to `this->rank()`.
    /// @param sel_op One of selection operators.
    /// - `H5S_SELECT_SET`: Replaces the existing selection with the parameters from this call.
    /// Overlapping blocks are not supported with this operator.
    /// - `H5S_SELECT_OR`: Adds the new selection to the existing selection.
    /// - `H5S_SELECT_AND`: Retains only the overlapping portions of the new selection and the
    /// existing selection.
    /// - `H5S_SELECT_XOR`: Retains only the elements that are members of the new selection or the
    /// existing selection, excluding elements that are members of both selections.
    /// - `H5S_SELECT_NOTB`: Retains only elements of the existing selection that are not in the new
    /// selection.
    /// - `H5S_SELECT_NOTA`: Retains only elements of the new selection that are not in the existing
    /// selection.
    /// @param start The offset of the starting element of the specified hyperslab.
    /// @param count The number of blocks to select from the dataspace, in each dimension.
    /// @param stride Determines how many elements to move in each dimension. Stride values of 0 are
    /// not allowed.
    /// @param block Determines the size of the element block selected from the dataspace. Block
    /// values of 0 are not allowed.
    ///
    void select(H5S_seloper_t sel_op, Extent const &start, Extent const &count,
                Extent const &stride, Extent const &block)
    {
        select(sel_op, start, count, &stride, &block);
    }

    /// Select hyperslab space.
    /// @details In this version, the block sizes are assumed to be all 1's.
    /// @see select
    ///
    void select(H5S_seloper_t sel_op, Extent const &start, Extent const &count,
                Extent const &stride)
    {
        select(sel_op, start, count, &stride, nullptr);
    }

    /// Select hyperslab space.
    /// @details In this version, the block sizes and strides are assumed to be all 1's.
    /// @see select
    ///
    void select(H5S_seloper_t sel_op, Extent const &start, Extent const &count)
    {
        select(sel_op, start, count, nullptr, nullptr);
    }
    /// @}
private:
    void select(H5S_seloper_t sel_op, Extent const &start, Extent const &count,
                Extent const *stride, Extent const *block);

public:
    /// @name Make simple space
    /// @{

    /// Create a scalar dataspace.
    ///
    [[nodiscard]] static auto scalar() { return Space{ H5S_SCALAR }; }

    /// Construct simple Space object of the given dimension sizes with the maximum dimension sizes.
    /// @param dims The size of each dimension of the dataset. Any elements can be 0.
    /// @param max_dims The upper limit on the size of each dimension.
    /// No element of `max_dims` should be smaller than the corresponding element of `dims`.
    /// If an element of `max_dims` is `H5S_UNLIMITED`, the maximum size of the corresponding
    /// dimension is unlimited.
    ///
    [[nodiscard]] static Space simple(Extent const &dims, Extent const &max_dims)
    {
        return simple(dims, &max_dims);
    }

    /// Construct simple Space object of the given dimension sizes with an unbounded maximum
    /// dimension sizes.
    /// @note The meaning of nullptr is different from the HDF5 library's `H5Ssimple_create` where
    /// nullptr indicates the upper limit the same as the dimension sizes. This is shorthand for
    /// `Space::simple(dims, {H5S_UNLIMITED, ...})`.
    /// @see simple
    ///
    [[nodiscard]] static Space simple(Extent const &dims, decltype(nullptr))
    {
        return simple(dims, Extent::unlimited(dims.rank()));
    }

    /// Construct simple Space object of the given dimension sizes.
    /// @details The upper limit is the same as the dimension sizes.
    /// @see simple
    ///
    [[nodiscard]] static Space simple(Extent const &dims) { return simple(dims, dims); }
    /// @}
private:
    [[nodiscard]] static Space simple(Extent const &dims, Extent const *max_dims);
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5Space_h */

/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5Object.h"
#include "println.h"

#include <iostream>
#include <stdexcept>
#include <string>

HDF5KIT_NAMESPACE_BEGIN(1)

Object::~Object()
{
    if (m.deleter && m.deleter(m.handle) < 0) {
        println(std::cerr, typeid(*this).name(), "::", __PRETTY_FUNCTION__,
                " - failed to close h5 object");
    }
}
Object::Object(hid_t handle, deleter_type deleter)
{
    if (handle < 0) {
        throw std::invalid_argument{ std::string{ typeid(*this).name() }
                                     + "::" + __PRETTY_FUNCTION__ + " - invalid hid handle" };
    }
    m = { handle, deleter };
}

HDF5KIT_NAMESPACE_END(1)

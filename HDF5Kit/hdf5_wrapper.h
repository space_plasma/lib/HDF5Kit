/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#pragma once

#if defined(__APPLE__)
#include <hdf5/hdf5.h>
#else
#include <hdf5.h>
#endif

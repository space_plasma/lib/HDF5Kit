/*
 * Copyright (c) 2017-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5Object_h
#define HDF5Kit_H5Object_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <utility>

#include <HDF5Kit/hdf5_wrapper.h>

HDF5KIT_NAMESPACE_BEGIN(1)

/// Wrapper for HDF5 generic object.
///
class Object {
public: // types
    /// Typedef of the object free function.
    ///
    typedef herr_t (*deleter_type)(hid_t);

public: // accessors
    /// Check whether `*this` is valid.
    ///
    explicit operator bool() const noexcept { return m.handle >= 0; }

    /// Return the native `hid_t` object.
    ///
    [[nodiscard]] auto operator*() const noexcept { return m.handle; }

protected: // for subclasses
    ~Object();
    Object() noexcept = default;
    Object(hid_t handle, decltype(nullptr)) noexcept : m{ handle, nullptr } {}
    Object(hid_t handle, deleter_type deleter); // exception when handle < 0
    Object(Object &&other) noexcept { std::swap(this->m, other.m); }
    Object &operator=(Object &&other) noexcept
    {
        std::swap(this->m, other.m);
        return *this;
    }

private: // member variables
    struct {
        hid_t        handle{ -1 };
        deleter_type deleter{ nullptr };
    } m{};
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5Object_h */

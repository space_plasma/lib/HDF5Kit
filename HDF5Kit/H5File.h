/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5File_h
#define HDF5Kit_H5File_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/H5Group.h>

#include <string>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class PList;

/// Wrapper for HDF5 file.
///
class File final : public Group {
    // file creation tag
    struct create_tag {
        unsigned const flag;
    };
    // file opening tag
    struct open_tag {
        unsigned const flag;
    };

public:
    /// @name file access flags
    /// @{

    /// Tag to indicate truncate file, if it already exists, erasing all data previously stored in
    /// the file.
    /// @note Only used in file creation context.
    ///
    struct trunc_tag : public create_tag {
        trunc_tag() noexcept;
    };

    /// Tag to indicate exclusive file creation; Fail if file already exists.
    /// @note Only used in file creation context.
    ///
    struct excl_tag : public create_tag {
        excl_tag() noexcept;
    };

    /// Tag to indicate read and write access on file opening.
    /// @note Only used in file opening context.
    ///
    struct rdwr_tag : public open_tag {
        rdwr_tag() noexcept;
    };

    /// Tag to indicate read-only access on file opening.
    /// @note Only used in file opening context.
    ///
    struct rdonly_tag : public open_tag {
        rdonly_tag() noexcept;
    };
    /// @}

public:
    /// @{
    File() noexcept        = default;
    File(File &&) noexcept = default;
    File &operator=(File &&) noexcept = default;
    /// @}

    /// Construct File object by creating HDF5 file at filepath.
    ///
    File(create_tag const &tag, char const *filepath, PList const &fapl = PList{},
         PList const &fcpl = PList{});

    /// Construct File object by opening HDF5 file at filepath.
    ///
    File(open_tag const &tag, char const *filepath, PList const &fapl = PList{});

public:
    /// @name file attributes
    /// @{

    /// Full path of HDF5 file representation in the filesystem.
    ///
    [[nodiscard]] std::string file_path() const;

    /// Creation PList of `*this`.
    ///
    [[nodiscard]] PList cpl() const;

    /// Access PList of `*this`.
    ///
    [[nodiscard]] PList apl() const;
    /// @}
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5File_h */

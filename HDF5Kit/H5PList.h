/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef HDF5Kit_H5PList_h
#define HDF5Kit_H5PList_h

#include <HDF5Kit/HDF5Kit-config.h>

#include <HDF5Kit/Extent.h>
#include <HDF5Kit/H5Object.h>

HDF5KIT_NAMESPACE_BEGIN(1)

// forward decls
//
class Attribute;
class Dataset;
class Group;
class File;
class Type;

/// Wrapper for HDF5 property list.
///
class PList final : public Object {
    friend Attribute;
    friend Dataset;
    friend Group;
    friend File;
    friend Type;

    PList(decltype(nullptr), hid_t handle);

public:
    /// @{
    PList(PList &&) noexcept = default;
    PList &operator=(PList &&) noexcept = default;
    PList(PList const &other);
    PList &operator=(PList const &other) { return *this = PList{ other }; }
    /// @}

    /// Construct default PList object.
    /// @details Wrapper for `H5P_DEFAULT`.
    ///
    PList() noexcept : Object{ H5P_DEFAULT, nullptr } {}

    /// Construct PList object of the given class.
    ///
    explicit PList(hid_t plist_class);

public: // plist classes
    // Datatype plists
    //
    /// Create datatype creation PList.
    ///
    [[nodiscard]] static PList tcpl() { return PList{ H5P_DATATYPE_CREATE }; }

    /// Create datatype access PList.
    ///
    [[nodiscard]] static PList tapl() { return PList{ H5P_DATATYPE_ACCESS }; }

    // Attribute plists
    //
    /// Create attribute creation PList.
    ///
    [[nodiscard]] static PList acpl() { return PList{ H5P_ATTRIBUTE_CREATE }; }

    // Group plists
    //
    /// Create group creation PList.
    ///
    [[nodiscard]] static PList gcpl() { return PList{ H5P_GROUP_CREATE }; }

    /// Create group access PList.
    ///
    [[nodiscard]] static PList gapl() { return PList{ H5P_GROUP_ACCESS }; }

    // File plists
    //
    /// Create file creation PList.
    ///
    [[nodiscard]] static PList fcpl() { return PList{ H5P_FILE_CREATE }; }

    /// Create file access PList.
    ///
    [[nodiscard]] static PList fapl() { return PList{ H5P_FILE_ACCESS }; }

    // Link plists
    //
    /// Create link creation PList.
    ///
    [[nodiscard]] static PList lcpl() { return PList{ H5P_LINK_CREATE }; }

    /// Create link access PList.
    ///
    [[nodiscard]] static PList lapl() { return PList{ H5P_LINK_ACCESS }; }

    // Dataset plists
    //
    /// Create dataset creation PList.
    ///
    [[nodiscard]] static PList dcpl() { return PList{ H5P_DATASET_CREATE }; }

    /// Create dataset access PList.
    ///
    [[nodiscard]] static PList dapl() { return PList{ H5P_DATASET_ACCESS }; }

    /// Create dataset transfer PList.
    ///
    [[nodiscard]] static PList dxpl() { return PList{ H5P_DATASET_XFER }; }

    /// Set chunk sizes of dataset creation PList.
    /// @exception Rank of the Extent must be positive.
    ///
    void set_chunk(Extent const &dims);

    /// Get chunk sizes of dataset creation PList.
    ///
    [[nodiscard]] Extent chunk() const;
};

HDF5KIT_NAMESPACE_END(1)

#endif /* HDF5Kit_H5PList_h */

/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5Attribute.h"
#include "H5PList.h"
#include "H5Space.h"
#include "H5Type.h"

#include <memory>
#include <stdexcept>

HDF5KIT_NAMESPACE_BEGIN(1)

Attribute::Attribute(decltype(nullptr), hid_t handle) : Object{ handle, &H5Aclose }
{
}
Attribute::Attribute(Object const &parent, char const *name, Type const &type, Space const &space,
                     PList const &aapl, PList const &acpl)
: Attribute{ nullptr, H5Acreate2(*parent, name, *type, *space, *acpl, *aapl) }
{
}
Attribute::Attribute(Object const &parent, char const *name, PList const &aapl)
: Attribute{ nullptr, H5Aopen(*parent, name, *aapl) }
{
}

auto Attribute::name() const -> std::string
{
    auto const _len = H5Aget_name(**this, 0, nullptr);
    if (_len < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Aget_name returned error" };
    }
    auto const len  = static_cast<unsigned long>(_len + 1);
    auto       name = std::make_unique<char[]>(len);
    if (H5Aget_name(**this, len, name.get()) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Aget_name returned error" };
    }
    return name.get();
}
auto Attribute::space() const -> Space
{
    if (hid_t id = H5Aget_space(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Aget_space returned error" };
    } else
        return { nullptr, id };
}
auto Attribute::type() const -> Type
{
    if (hid_t id = H5Aget_type(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Aget_type returned error" };
    } else
        return { nullptr, id };
}
auto Attribute::cpl() const -> PList
{
    if (hid_t id = H5Aget_create_plist(**this); id < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Aget_create_plist returned error" };
    } else
        return { nullptr, id };
}

void Attribute::flush()
{
    if (H5Fflush(**this, H5F_SCOPE_LOCAL) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Fflush returned error" };
    }
}
void Attribute::write(void const *data, Type const &type)
{
    if (H5Awrite(**this, *type, data) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Awrite returned error" };
    }
}
void Attribute::read(void *buffer, Type const &type) const
{
    if (H5Aread(**this, *type, buffer) < 0) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - H5Aread returned error" };
    }
}

HDF5KIT_NAMESPACE_END(1)

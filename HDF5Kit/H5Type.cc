/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "H5Type.h"
#include "H5PList.h"

#include <limits>
#include <numeric>
#include <stdexcept>
#include <string>

HDF5KIT_NAMESPACE_BEGIN(1)

Type::Type(decltype(nullptr), hid_t handle) : Object{ handle, &H5Tclose }
{
}
Type::Type(Type const &o) : Object{}
{
    if (o) { // protect against copying invalid object
        *this = { nullptr, H5Tcopy(*o) };
    }
}
Type::Type(H5T_class_t type_class, unsigned long size)
: Type{ nullptr, H5Tcreate(type_class, size) }
{
}

bool Type::is_committed() const
{
    return H5Tcommitted(**this);
}

H5T_class_t Type::class_() const
{
    return H5Tget_class(**this);
}

PList Type::cpl() const
{
    return { nullptr, H5Tget_create_plist(**this) };
}

unsigned Type::size() const
{
    if (auto const size = H5Tget_size(**this); size <= std::numeric_limits<unsigned>::max()) {
        return static_cast<unsigned>(size);
    }
    throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                             + " - H5Tget_size returned size that cannot fit in unsigned int" };
}

Type Type::super_() const
{
    return { nullptr, H5Tget_super(**this) };
}

Type Type::native(H5T_direction_t dir) const
{
    return { nullptr, H5Tget_native_type(**this, dir) };
}

H5T_order_t Type::order() const
{
    return H5Tget_order(**this);
}

Type Type::enumeration() const
{
    return { nullptr, H5Tenum_create(**this) };
}

Type Type::vlen() const
{
    return { nullptr, H5Tvlen_create(**this) };
}

Type Type::array(Extent const &dims) const
{
    if (0 == dims.rank()) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - invalid rank for array datatype" };
    }
    if (std::accumulate(dims.begin(), dims.end(), bool{},
                        [](bool const &lhs, hsize_t const &rhs) -> bool {
                            return lhs | (0 == rhs);
                        })) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - null dimension size for array datatype" };
    }
    return { nullptr, H5Tarray_create2(**this, dims.rank(), dims.data()) };
}

void Type::insert(char const *name, unsigned long offset, Type const &type)
{
    if (!type)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid field type" };

    if (H5Tinsert(**this, name, offset, *type) < 0)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - H5Tinsert of the field `"
                                  + name + "' failed" };
}

HDF5KIT_NAMESPACE_END(1)

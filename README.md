# HDF5Kit

[![pipeline status](https://gitlab.com/space_plasma/lib/HDF5Kit/badges/master/pipeline.svg)](https://gitlab.com/space_plasma/lib/HDF5Kit/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)


HDF5Kit is a thin layer on top of the hdf5 C library, providing a basic subset of APIs with C++ abstraction.
This library is meant to reduce boilerplate codes.

The use of this library requires a **C++** with a **C++17** support and hdf5 library with version 1.8 or above.
The build configurations are found at [HDF5Kit/Makefile](HDF5Kit/Makefile).

Example use cases are found at [HDF5KitTests+File.mm](HDF5KitTests/HDF5KitTests+File.mm).


## API Documentation

API is documented using doxygen markups (also available [here](https://space_plasma.gitlab.io/lib/HDF5Kit)).

To build documentation, execute `make docs` in the project's root directory.
By default, it will create documentation in the `html` format.
To produce the documentation in other formats, modify the Doxygen configuration, [Doxyfile](Doxyfile).

For details about providing user-defined typemaps, refer to [HDF5Kit/TypeMap.h](HDF5Kit/TypeMap.h).

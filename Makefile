export CXX=/usr/bin/xcrun --sdk macosx clang++ -stdlib=libc++
export LIBTOOL=/usr/bin/xcrun --sdk macosx libtool  -static -o
#export g++
#export LIBTOOL=ar rcs

export CFLAGS=-std=c++17 -O3 -F/Library/com.kyungguk.Frameworks.localized
export LDFLAGS=-O3 -flto #-F/Library/com.kyungguk.Frameworks.localized -framework hdf5
#export CFLAGS=-std=c++17 -O3
#export LDFLAGS=-O3 -flto -lpthread

TARGETS=./HDF5Kit/HDF5Kit.a

DOXY_CONFIG=Doxyfile

HEADERS=$(shell find ./HDF5Kit -type f -name "*.h") \
		$(shell find ./HDF5Kit -type f -name "*.hh")

all: $(TARGETS)

%.a:
	+make -eC $(dir $@) $(notdir $@)

docs: $(HEADERS) $(DOXY_CONFIG)
	doxygen $(DOXY_CONFIG)

clean:
	rm -rf docs
	$(foreach var, $(TARGETS), \
		@make -C $(dir $(var)) clean \
		)
